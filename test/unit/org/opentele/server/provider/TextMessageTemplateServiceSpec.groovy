package org.opentele.server.provider



import grails.test.mixin.*
import grails.test.mixin.gorm.Domain
import grails.test.mixin.hibernate.HibernateTestMixin
import org.opentele.server.core.exception.EntityNotFoundException
import org.opentele.server.model.TextMessageTemplate
import spock.lang.Specification

@TestFor(TextMessageTemplateService)
@Domain(TextMessageTemplate)
@TestMixin(HibernateTestMixin)
class TextMessageTemplateServiceSpec extends Specification{

    def "can create valid text message template"() {
        when:
        def saved = service.createTemplate("some name", "some content")

        then:
        saved.hasErrors() == false
        TextMessageTemplate.get(saved.id) != null
    }

    def "if text message template fails validation create template fails"() {
        when:
        def notSaved = service.createTemplate(null, "")

        then:
        notSaved.hasErrors() == true
        notSaved.id == null
    }

    def "can update existing text message template"() {
        given:
        def toUpdate = new TextMessageTemplate(name: "bla", content: "bla").save(failOnError: true, flush: true)

        when:
        def updated = service.updateTemplate(toUpdate.id, toUpdate.version, "updated name", "updated content")

        then:
        updated.hasErrors() == false
        TextMessageTemplate.get(toUpdate.id).name == "updated name"
        TextMessageTemplate.get(toUpdate.id).content == "updated content"
    }

    def "if trying to update non-existing text message template exception is thrown"() {
        when:
        service.updateTemplate(42, 1, "bla", "bla")

        then:
        thrown(EntityNotFoundException)
    }

    def "if trying to update existing text message template with version mismatch update fails"() {
        given:
        def toUpdate = new TextMessageTemplate(name: "bla", content: "bla").save(failOnError: true)
        toUpdate.name = "force version number to be upped"
        toUpdate.save(flush: true)

        when:
        def updated = service.updateTemplate(toUpdate.id, 0, "updated name", "updated content")

        then:
        updated.hasErrors() == true
        updated.name == "force version number to be upped"
    }

    def "if trying to update existing text message template with invalid values update fails"() {
        given:
        def toUpdate = new TextMessageTemplate(name: "bla", content: "bla").save(failOnError: true, flush: true)

        when:
        def updated = service.updateTemplate(toUpdate.id, toUpdate.version, null, "updated content")

        then:
        updated.hasErrors() == true
        TextMessageTemplate.findByName("bla") != null
    }

    def "can delete text message template"() {
        given:
        def toDelete = new TextMessageTemplate(name: "bla", content: "bla").save(failOnError: true, flush: true)

        when:
        service.deleteTemplate(toDelete.id)

        then:
        TextMessageTemplate.get(toDelete.id) == null
    }

    def "trying to delete non-existing template throws exception"() {
        when:
        service.deleteTemplate(42)

        then:
        thrown(EntityNotFoundException)
    }
}
