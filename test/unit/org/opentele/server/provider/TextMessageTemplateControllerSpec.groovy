package org.opentele.server.provider



import grails.test.mixin.*
import org.opentele.server.core.exception.EntityNotFoundException
import org.opentele.server.model.TextMessageTemplate
import spock.lang.Specification

@TestFor(TextMessageTemplateController)
@Mock(TextMessageTemplate)
class TextMessageTemplateControllerSpec extends Specification{

    def textMessageTemplateService

    def setup() {
        textMessageTemplateService = Mock(TextMessageTemplateService)
        controller.textMessageTemplateService = textMessageTemplateService
    }

    def "index action is redirected to list action"() {
        when:
        controller.index()

        then:
        response.redirectedUrl == "/textMessageTemplate/list"
    }

    def "can show first page of text message templates"() {
        given:
        createTextMessageTemplates(12)

        when:
        def viewModel = controller.list()

        then:
        viewModel.templateList.size() == 10
        viewModel.templateList.first().name == "name_1"
        viewModel.templateList.last().name == "name_10"
        viewModel.templateListTotal == 12
    }

    def "can show next page of text message templates"() {
        given:
        createTextMessageTemplates(12)

        when:
        params.offset = 10
        def viewModel = controller.list()

        then:
        viewModel.templateList.size() == 2
        viewModel.templateList.first().name == "name_11"
        viewModel.templateList.last().name == "name_12"
        viewModel.templateListTotal == 12
    }

    def "can show specific text message template"() {
        given:
        createTextMessageTemplates(3)

        when:
        params.id = "2"
        def viewModel = controller.show()

        then:
        viewModel.template.name == "name_2"
        viewModel.template.content == "content_2"
    }

    def "redirects to list action and shows error message if specific template requested is not found"() {
        when:
        params.id = "42"
        controller.show()

        then:
        response.redirectedUrl == "/textMessageTemplate/list"
        flash.message.contains("not.found")
    }

    def "can request to create new text message template"() {
        when:
        def viewModel = controller.create()

        then:
        viewModel.template != null
        viewModel.template.name == null
        viewModel.template.content == null
    }

    def "can save new text message template"() {
        given:
        params.name = "some name"
        params.content = "some content"

        when:
        controller.save()

        then:
        1 * textMessageTemplateService.createTemplate(params.name, params.content) >> new TextMessageTemplate(params)
        flash.message.contains("created")
        response.redirectedUrl == "/textMessageTemplate/list"
    }

    def "when trying to save template create view is re-rendered if validation fails during save"() {
        given:
        def invalidTemplate = new TextMessageTemplate()
        invalidTemplate.validate()
        textMessageTemplateService.createTemplate(_, _) >> invalidTemplate

        when:
        controller.save()

        then:
        view == "/textMessageTemplate/create"
        model.template == invalidTemplate
    }

    def "can request to edit existing text message template"() {
        given:
        createTextMessageTemplates(2)
        def toEdit = TextMessageTemplate.findAll().first()

        when:
        params.id = toEdit.id.toString()
        def viewModel = controller.edit()

        then:
        viewModel.template == toEdit
    }

    def "when trying to edit non-existing template user is redirected to list and message is shown"() {
        when:
        params.id = "42"
        controller.edit()

        then:
        response.redirectedUrl == "/textMessageTemplate/list"
        flash.message.contains("not.found")
    }

    def "can update text message template"() {
        given:
        createTextMessageTemplates(2)
        def toUpdate = TextMessageTemplate.findAll().first()

        when:
        params.id = toUpdate.id.toString()
        params.version = toUpdate.version.toString()
        params.name = "updated name"
        params.content = "updated content"
        controller.update()

        then:
        1 * textMessageTemplateService.updateTemplate(toUpdate.id, toUpdate.version, "updated name", "updated content") >> toUpdate
        response.redirectedUrl == "/textMessageTemplate/show/${toUpdate.id}"
        flash.message.contains("updated")
    }

    def "when trying to update template edit view is re-rendered if validation fails during save" () {
        given:
        def invalidTemplate = new TextMessageTemplate()
        invalidTemplate.validate()
        textMessageTemplateService.updateTemplate(_, _, _, _) >> invalidTemplate

        when:
        params.id = "42"
        params.version = "1"
        controller.update()

        then:
        view == "/textMessageTemplate/edit"
        model.template == invalidTemplate
    }

    def "if template has been deleted by another user while editing user is redirected to list and message shown"() {
        given:
        textMessageTemplateService.updateTemplate(_, _, _, _) >> { throw new EntityNotFoundException() }

        when:
        params.id = "42"
        params.version = "1"
        controller.update()

        then:
        response.redirectedUrl == "/textMessageTemplate/list"
        flash.message.contains("textMessageTemplate.deleted.by.other")
    }

    def "can delete text message template"() {
        given:
        createTextMessageTemplates(2)
        def toDelete = TextMessageTemplate.findAll().first()

        when:
        params.id = toDelete.id.toString()
        controller.delete()

        then:
        1 * textMessageTemplateService.deleteTemplate(toDelete.id)
        response.redirectedUrl == "/textMessageTemplate/list"
        flash.message.contains("default.deleted.message")
    }

    def "if trying to delete message template already deleted user is redirected to list and message shown"() {
        given:
        textMessageTemplateService.deleteTemplate(_) >> { throw new EntityNotFoundException()}

        when:
        params.id = "42"
        controller.delete()

        then:
        response.redirectedUrl == "/textMessageTemplate/list"
        flash.message.contains("not.found")
    }

    private def createTextMessageTemplates(int noToCreate) {
        noToCreate.times {idx ->
            new TextMessageTemplate(name: "name_${idx + 1}", content: "content_${idx + 1}").save(failOnError: true)
        }
    }
}
