<td><g:message code="conferenceMeasurement.bloodSugar.confirmation.title"/></td>
<td>
    <span>
        <div>
            <g:message code="conferenceMeasurement.bloodSugar.confirmation.bloodSugar"
                       args="[g.formatNumber(number: measurement.value, format:'0.0')]"/>
        </div>
    </span>
</td>
