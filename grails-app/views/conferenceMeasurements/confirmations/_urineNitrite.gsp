<td><g:message code="conferenceMeasurement.urineNitrite.confirmation.title"/></td>
<td>
    <span>
        <div>
            <g:message code="conferenceMeasurement.urineNitrite.confirmation.urineNitrite"
                       args="[measurement.nitriteInUrine]"/>
        </div>
    </span>
</td>
