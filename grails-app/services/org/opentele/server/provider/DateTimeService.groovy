package org.opentele.server.provider

import org.springframework.web.servlet.support.RequestContextUtils

import java.text.DateFormat
import java.text.ParseException

class DateTimeService {


    def uses12HourClock(request) {
        Locale currentLocale = getCurrentLocale(request)
        return uses12HourClock(currentLocale)
    }

    def uses12HourClock(Locale currentLocale) {
        def stdFormat = DateFormat.getTimeInstance(DateFormat.SHORT, Locale.US)
        def localeFormat = DateFormat.getTimeInstance(DateFormat.LONG, currentLocale)

        String midnight
        try {
            midnight = localeFormat.format(stdFormat.parse("12:00 AM"))
        } catch (ParseException ignore) {
            return false
        }
        return midnight.contains("12")
    }

    private getCurrentLocale(request) {
        return RequestContextUtils.getLocale(request)
    }
}
