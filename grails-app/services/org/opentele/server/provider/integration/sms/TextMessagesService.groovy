package org.opentele.server.provider.integration.sms

import groovyx.net.http.ContentType
import groovyx.net.http.HTTPBuilder
import groovyx.net.http.Method
import org.codehaus.groovy.grails.web.json.JSONObject
import org.opentele.server.provider.Exception.FailedToSendSmsException
import org.opentele.server.provider.Exception.InvalidSmsIntegrationConfigurationException

class TextMessagesService {

    def grailsApplication

    def send(String phoneNumber, String message) {
        def url = gatewayUrlFromConfig()
        def http = new HTTPBuilder(url)

        log.debug("Sending SMS using gateway ${url}")

        http.request(Method.GET, ContentType.JSON) {
            uri.query = [number: phoneNumber, text: message]
            response.success = {resp, json ->
                log.debug("SMS Gateway replied with status: ${resp.statusLine}")
                log.debug("SMS Gateway replied with body: ${json}")

                if (json?.succes != 1) {
                    log.error("Received error reply from SMS gateway")
                    throw new FailedToSendSmsException("Received error reply from SMS gateway")
                }
            }
            response.failure = { resp, json ->
                log.error("Error while trying to send SMS. Status code: ${resp.statusLine}")
                throw new FailedToSendSmsException(resp.statusLine.toString())
            }
        }
    }

    private gatewayUrlFromConfig() {
        def enabled = Boolean.valueOf(grailsApplication.config.sms.enabled)
        if (!enabled) {
            throw new InvalidSmsIntegrationConfigurationException("SMS integration not enabled")
        }
        try {
            return new URL(grailsApplication.config.sms.gateway)
        } catch (MalformedURLException ex) {
            throw new InvalidSmsIntegrationConfigurationException(ex)
        }
    }
}
