package org.opentele.server.provider

import org.opentele.server.core.exception.EntityNotFoundException
import org.opentele.server.model.TextMessageTemplate

class TextMessageTemplateService {

    def createTemplate(String name, String content) {
        def template = new TextMessageTemplate(name: name, content: content)
        template.save()
        template
    }

    def updateTemplate(long id, long expectedVersion, String name, String content) {
        def template = TextMessageTemplate.get(id)
        if (!template) {
            throw new EntityNotFoundException()
        }

        if (template.version > expectedVersion) {
            template.errors.rejectValue("version", "textMessageTemplate.optimistic.locking.failure")
            return template
        }

        template.name = name
        template.content = content
        template.save()
        template
    }

    def deleteTemplate(long id) {
        def template = TextMessageTemplate.get(id)
        if (!template) {
            throw new EntityNotFoundException()
        }

        template.delete()
    }
}
